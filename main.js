//local storage

if (typeof(Storage) !== "undefined") {
  // Code for localStorage/sessionStorage.
} else {
  window.alter("Local storage is not supported");
}

$(document).ready(function () {
  console.log('Ready')
  $('.submit').click(function (event) {
    
    console.log('Button has been clicked')

    var email = $('.email').val()
    var subject = $('.subject').val()
    var message = $('.message').val()
    var statusElm = $('.status')
    statusElm.empty()

    if(email.length > 5 && email.includes('@') && email.includes('.')) {
      statusElm.append('<div>Email is valid</div>')
    }

    else {
      event.preventDefault()
      console.log("Email does not meet requirements")
    }

    if(subject.length > 2) {
        statusElm.append('<div>Subject is valid</div>')
    }

    else {
      event.preventDefault()
      statusElm.append('<div>Subject is NOT valid</div>')
    }

    if(message.length > 20) {
      statusElm.append('<div>Message is valid</div>')
    }

    else {
      event.preventDefault()
      statusElm.append('<div>Email is NOT valid</div>')
    }
  })
})

function check(){

	var question1 = document.quiz.question1.value;
	var question2 = document.quiz.question2.value;
	var question3 = document.quiz.question3.value;
  var question4 = document.quiz.question4.value;
  var question5 = document.quiz.question5.value;
	var correct = 0;


	if (question1 == "2") {
		correct++;
    jQuery("#question1").css("color", "green");
}

  else {
    jQuery("#question1").css("color", "red");
}

	if (question2 == "2450") {
		correct++;
    jQuery("#question2").css("color", "green");
}

else {
  jQuery("#question2").css("color", "red");
}

	if (question3 == "27") {
		correct++;
    jQuery("#question3").css("color", "green");
	}

else {
    jQuery("#question3").css("color", "red");
}

if (question4 == "164") {
  correct++;
  jQuery("#question4").css("color", "green");
}

else {
    jQuery("#question4").css("color", "red");
}

if (question5 == "24800") {
  correct++;
  jQuery("#question5").css("color", "green");
}

else {
    jQuery("#question5").css("color", "red");
}

	var score;

	if (correct == 0) {
		score = 2;
	}

	if (correct > 0 && correct < 3) {
		score = 1;
	}

	if (correct == 3) {
		score = 0;
	}

  //will only output if has already been initialised
  if(localStorage.getItem("result") === null) {
  }

  else {
    document.getElementById("output").innerHTML = "Last attempt you got " + localStorage.getItem("result") + " Correct";
  }

  localStorage.setItem("result", correct);

	document.getElementById("after_submit").style.visibility = "visible";
	document.getElementById("number_correct").innerHTML = "You got " + correct + " correct.";

	}
